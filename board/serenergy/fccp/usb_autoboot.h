/*
 * (C) Copyright 2014 Serenergy A/S
 * Author: Steffen Malmgaard Mortensen <smm@serenergy.com>
 *
 * Based on work by:
 *
 * (C) Copyright 2007 Schindler Lift Inc.
 *
 * Author: Michel Marti <mma@objectxp.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __USB_AUTOBOOT_H
#define __USB_AUTOBOOT_H

/* Default prefix for output messages */
#define LOG_PREFIX	"FCCP:"

/* Extra debug macro */
#ifdef CONFIG_USBI_AUTOBOOT_DEBUG
#define USB_AUTOBOOT_DEBUG(fmt, args...) printf(LOG_PREFIX fmt, ##args)
#else
#define USB_AUTOBOOT_DEBUG(fmt, args...)
#endif

/* Name of the directory holding firmware images */
#define USB_AUTOBOOT_FITIMAGE		"UpgradeInstaller.img"
#define AUTO_BOOTARGS "console=%s,%s %s"

/* Main function for usbboot */
void fccp_usb_autoboot(void);

#endif /* __USB_AUTOBOOT_H */
