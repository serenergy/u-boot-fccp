/*
 * (C) Copyright 2017 Serenergy A/S
 * (C) Copyright 2007 Schindler Lift Inc.
 * (C) Copyright 2007 DENX Software Engineering
 *
 * Author: Steffen Malmgaard Mortensen <smm@serenergy.com>
 *
 * Based on work by:
 * Author: Michel Marti <mma@objectxp.com>
 * Adapted for U-Boot 1.2 by Piotr Kruszynski <ppk@semihalf.com>:
 *   - code clean-up
 *   - bugfix for overwriting bootargs by user
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <image.h>
#include <fs.h>
#include <linux/ctype.h>

#include "mmc_autoboot.h"

DECLARE_GLOBAL_DATA_PTR;

static int load_image(ulong mmcdev, ulong mmcpart, const char*, ulong);
static int check_for_mmc_storage_and_dir(ulong mmcdev, ulong mmcpart);

static char fwdir[64];

static int devno;
static int partno;

void fccp_mmc_autoboot(void)
{
    cmd_tbl_t *bcmd;
    char *autoargs;
    char *tmp = NULL;
    char conf[30];

    char * const argv[5] = { "bootm", conf, NULL };

    /* Check if rescue system is disabled... */
    if (env_get("norescue")) {
        printf(LOG_PREFIX "Rescue System disabled.\n");
        return;
    }

    ulong mmcdev=env_get_ulong("mmcdev", 10, 0);
    ulong mmcpart=env_get_ulong("mmcpart", 10, 2);

    /* Check if we have a MMC storage device */
    if (check_for_mmc_storage_and_dir(mmcdev, mmcpart)){
        return;
    }

    ulong loadaddr = env_get_hex("loadaddr", 0xffffffff);
    if (loadaddr == 0xffffffff){
        return;
    }

    if (load_image(mmcdev, mmcpart, MMC_AUTOBOOT_FITIMAGE, loadaddr)){
        return;
    }
    if (gd->ram_size == (128*1024*1024))
    {
        sprintf(conf, "%lx#conf@1", loadaddr);
    } else {
        sprintf(conf, "%lx#conf@2", loadaddr);
    }

    /* make sure mtdparts var is defined */
    mtdparts_init();

    bcmd = find_cmd("bootm");
    if (!bcmd)
        return;

    /* prepare our bootargs */
    autoargs = env_get("auto-args");
    if (!autoargs){
        char *console = env_get("console_mainline");
        char *baudrate = env_get("baudrate");
        char *mtdparts = env_get("mtdparts");

        tmp = malloc(256);
        if (!tmp) {
            printf(LOG_PREFIX "Memory allocation failed\n");
            return;
        }
        sprintf(tmp, MMC_AUTOBOOT_ARGS, console, baudrate, mtdparts);
        printf(LOG_PREFIX "%s - %s - %s\n", console, baudrate, tmp);
        autoargs = tmp;
    }else {
        printf("using auto-args from env (%s)\n", autoargs);
    }

    env_set("bootargs", autoargs);

    printf(LOG_PREFIX "Starting update system (bootargs=%s)...\n", autoargs);
    if (autoargs == tmp)
        free(autoargs);

    do_bootm(bcmd, 0, 4, argv);
}

static int check_for_mmc_storage_and_dir(ulong mmcdev, ulong mmcpart)
{
    char *tmp = NULL;
    char dev_part[16];
    snprintf(dev_part, sizeof(dev_part), "%d:%d", mmcdev, mmcpart);

    printf(LOG_PREFIX "Checking for upgrade image '%s' on MMC"
            " storage...\n",MMC_AUTOBOOT_FITIMAGE);

    /* Detect storage device */
    if (fs_set_blk_dev("mmc", dev_part, FS_TYPE_ANY)){
        printf(LOG_PREFIX "MMC %s not a valid storage device...\n", dev_part);
        goto failure;
    }

    MMC_AUTOBOOT_DEBUG("Looking for upgrade installer image on mmc partition %d\n", mmcpart);
    if (fs_exists(MMC_AUTOBOOT_FITIMAGE) == 0) {
        MMC_AUTOBOOT_DEBUG("No upgrade installer image found \n");
        goto failure;
    }

    fs_set_blk_dev("mmc", dev_part, FS_TYPE_ANY);
    MMC_AUTOBOOT_DEBUG("Looking for upgrade image '%s' on mmc partition %d\n", MMC_AUTOBOOT_UPDATE, mmcpart);
    if (fs_exists(MMC_AUTOBOOT_UPDATE) == 0) {
        MMC_AUTOBOOT_DEBUG("No upgrade image found \n");
        goto failure;
    }

    MMC_AUTOBOOT_DEBUG("Images found - continue mmc autoboot process\n");

    return 0;

failure:
    return 1;
}

static int load_image(ulong mmcdev, ulong mmcpart, const char* filename, ulong addr)
{
    loff_t filesize, actread;
    char dev_part[16];
    snprintf(dev_part, sizeof(dev_part), "%d:%d", mmcdev, mmcpart);

    MMC_AUTOBOOT_DEBUG("fs_read addr='%lx', file: %s\n", addr, filename);

    fs_set_blk_dev("mmc", dev_part, FS_TYPE_ANY);
    if (fs_size(filename, &filesize)<0){
        //fail
        return 1;
    }

    MMC_AUTOBOOT_DEBUG("fs_size: %s %d\n", filename, filesize);
    fs_set_blk_dev("mmc", dev_part, FS_TYPE_ANY);
    if (fs_read(filename, addr, 0, filesize, &actread) < 0){
        //fail
        return 1;
    }

    //success
    return 0;
}

