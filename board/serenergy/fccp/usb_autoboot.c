/*
 * (C) Copyright 2014 Serenergy A/S
 * (C) Copyright 2007 Schindler Lift Inc.
 * (C) Copyright 2007 DENX Software Engineering
 *
 * Author: Steffen Malmgaard Mortensen <smm@serenergy.com>
 *
 * Based on work by:
 * Author: Michel Marti <mma@objectxp.com>
 * Adapted for U-Boot 1.2 by Piotr Kruszynski <ppk@semihalf.com>:
 *   - code clean-up
 *   - bugfix for overwriting bootargs by user
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <image.h>
#include <usb.h>
#include <fat.h>
#include <linux/ctype.h>

#include "usb_autoboot.h"

DECLARE_GLOBAL_DATA_PTR;

extern int do_fat_read(const char *filename, void *buffer, loff_t maxsize, int dols, loff_t *actread);
extern int do_fat_fsload(cmd_tbl_t *, int, int, char * const []);

static int load_image(const char*, ulong);
static int check_for_usb_storage_and_dir(void);

static char fwdir[64];

static int devno;
static int partno;

void fccp_usb_autoboot(void)
{
	cmd_tbl_t *bcmd;
	char *autoargs;
	char *tmp = NULL;
	char conf[30];

	char * const argv[5] = { "bootm", conf, NULL };

	/* Check if rescue system is disabled... */
	if (env_get("norescue")) {
		printf(LOG_PREFIX "Rescue System disabled.\n");
		return;
	}

    /* Check if we have a USB storage device */

    if (check_for_usb_storage_and_dir()){
        return;
    }

    ulong loadaddr = env_get_hex("loadaddr", 0xffffffff);
    if (loadaddr == 0xffffffff){
        return;
    }

    if (load_image(USB_AUTOBOOT_FITIMAGE, loadaddr)){
        return;
    }
    if (gd->ram_size == (128*1024*1024))
    {
        sprintf(conf, "%lx#conf@1", loadaddr);
    } else {
        sprintf(conf, "%lx#conf@2", loadaddr);
    }

    /* make sure mtdparts var is defined */
    mtdparts_init();

	bcmd = find_cmd("bootm");
	if (!bcmd)
		return;

	/* prepare our bootargs */
	autoargs = env_get("auto-args");
	if (!autoargs){
        char *console = env_get("console_mainline");
        char *baudrate = env_get("baudrate");
        char *mtdparts = env_get("mtdparts");

		tmp = malloc(256);
		if (!tmp) {
			printf(LOG_PREFIX "Memory allocation failed\n");
			return;
		}
		sprintf(tmp, AUTO_BOOTARGS, console, baudrate, mtdparts);
        printf(LOG_PREFIX "%s - %s - %s\n", console, baudrate, tmp);
		autoargs = tmp;
    }else {
        printf("using auto-args from env (%s)\n", autoargs);
	}

	env_set("bootargs", autoargs);

	printf(LOG_PREFIX "Starting update system (bootargs=%s)...\n", autoargs);
	if (autoargs == tmp)
		free(autoargs);

	do_bootm(bcmd, 0, 4, argv);
}

static int check_for_usb_storage_and_dir(void)
{
	disk_partition_t info;
	int i;
	struct blk_desc *stor_dev = NULL;

	printf(LOG_PREFIX "Checking for upgrade image '%s' on USB"
		" storage...\n",USB_AUTOBOOT_FITIMAGE);
	usb_stop();

	ulong delay = env_get_ulong("usb_detection_delay", 10, 3);
	udelay(delay * 1000000);

	if (usb_init() != 0)
		return 1;

	/* Check for storage device */
	if (usb_stor_scan(1) != 0) {
		usb_stop();
		return 1;
	}

	/* Detect storage device */
	for (devno = 0; devno < USB_MAX_STOR_DEV; devno++) {
		stor_dev = blk_get_dev("usb", devno);
		if (stor_dev->type != DEV_TYPE_UNKNOWN)
			break;
	}
	if (!stor_dev || stor_dev->type == DEV_TYPE_UNKNOWN) {
		printf(LOG_PREFIX "No valid storage device found...\n");
		usb_stop();
		return 1;
	}

	/* Detect partition */
	for (partno = -1, i = 0; i < 6; i++) {
		if (part_get_info(stor_dev, i, &info) == 0) {
			if (fat_register_device(stor_dev, i) == 0) {
                loff_t size;
				/* Check if rescue image is present */
				USB_AUTOBOOT_DEBUG("Looking for upgrade image'%s'"
					" on partition %d\n", fwdir, i);
				if (fat_exists(USB_AUTOBOOT_FITIMAGE) == 0) {
					USB_AUTOBOOT_DEBUG("No upgrade image on "
						"partition %d.\n", i);
					partno = -2;
				} else {
					partno = i;
					USB_AUTOBOOT_DEBUG("Partition %d contains "
						"upgrade image\n", partno);
					break;
				}
			}
		}
	}

	if (partno < 0) {
		switch (partno) {
		case -1:
			printf(LOG_PREFIX "Error: No valid (FAT) partition "
				"detected\n");
			break;
		case -2:
			printf(LOG_PREFIX "Error: No upgrade image on FAT "
				"partition\n");
			break;
		default:
			printf(LOG_PREFIX "Error: Failed with code %d\n",
				partno);
		}
		usb_stop();
		return 1;
	}

    return 0;
}

static int load_image(const char* filename, ulong addr)
{
	char nxri[128];
	char dev[7];
	char addr_str[16];
	char * const argv[6] = { "fatload", "usb", dev, addr_str, nxri, NULL };
	cmd_tbl_t *bcmd;

	/* Load the rescue image */
	bcmd = find_cmd("fatload");
	if (!bcmd) {
		printf(LOG_PREFIX "Error - 'fatload' command not present.\n");
		usb_stop();
		return 1;
	}
	
    sprintf(nxri, "%s/%s", fwdir, filename);
	sprintf(dev, "%d:%d", devno, partno);
	sprintf(addr_str, "%lx", addr);

	USB_AUTOBOOT_DEBUG("fat_fsload device='%s', addr='%s', file: %s\n",
		dev, addr_str, nxri);

	if (do_fat_fsload(bcmd, 0, 5, argv) != 0) {
		usb_stop();
		return 1;
	}

	return 0;
}

