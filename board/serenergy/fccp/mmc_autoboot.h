/*
 * (C) Copyright 2017 Serenergy A/S
 * Author: Steffen Malmgaard Mortensen <smm@serenergy.com>
 *
 * Based on work by:
 *
 * (C) Copyright 2007 Schindler Lift Inc.
 *
 * Author: Michel Marti <mma@objectxp.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __MMC_AUTOBOOT_H
#define __MMC_AUTOBOOT_H

/* Default prefix for output messages */
#define LOG_PREFIX	"FCCP:"

/* Extra debug macro */
#ifdef CONFIG_MMC_AUTOBOOT_DEBUG
#define MMC_AUTOBOOT_DEBUG(fmt, args...) printf(LOG_PREFIX fmt, ##args)
#else
#define MMC_AUTOBOOT_DEBUG(fmt, args...)
#endif

/* Name of the directory holding firmware images */
#define MMC_AUTOBOOT_FITIMAGE		"UpgradeInstaller.img"
#define MMC_AUTOBOOT_UPDATE			"fccp.img"
#define MMC_AUTOBOOT_ARGS "console=%s,%s %s"

/* Main function for mmcboot */
void fccp_mmc_autoboot(void);

#endif /* __MMC_AUTOBOOT_H */
